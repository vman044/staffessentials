package net.jmdev.staffessentials.commands;

import net.jmdev.staffessentials.StaffEssentials;
import net.jmdev.staffessentials.util.ConfigUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/29/17 | 12:35 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class Report extends Command {

	public Report() {
		super("report", "bungeeessentials.report");
	}

	@Override
	public void execute(CommandSender commandSender, String[] args) {
		//Check if player's sending the message
		if (! (commandSender instanceof ProxiedPlayer)) {
			commandSender.sendMessage(new TextComponent("You must be a player to execute this command!"));
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer) commandSender;
		//Verify they typed the command correctly
		if (args.length > 1) {
			//Make sure the player exists
			if (ProxyServer.getInstance().getPlayer(args[0]) != null) {
				ProxiedPlayer reportedPlayer = ProxyServer.getInstance().getPlayer(args[0]);
				String reportedPlayerServer = reportedPlayer.getServer().getInfo().getName();
				String reporterServer = player.getServer().getInfo().getName();
				StringBuilder reason = new StringBuilder();
				for (int x = 1; x < args.length; x++) {
					reason.append(args[x] + " ");
				}
				StaffEssentials.getInstance().broadcastStaffMessage(ConfigUtil.getConfig().getString("reportMessage")
				.replace("%reporter%", player.getName())
				.replace("%reporterServer%", reporterServer)
				.replace("%reportedPlayer%", reportedPlayer.getName())
				.replace("%reportedPlayerServer%", reportedPlayerServer)
				.replace("%reason%", reason));
			} else {
				player.sendMessage(new TextComponent(ConfigUtil.getConfig().getString("reportNoSuchPlayer")));
			}

		} else {
			player.sendMessage(new TextComponent(ConfigUtil.getConfig().getString("reportUsageMessage")));
		}

	}
}
