package net.jmdev.staffessentials.commands;

import net.jmdev.staffessentials.StaffEssentials;
import net.jmdev.staffessentials.util.ConfigUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/29/17 | 6:59 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class Request extends Command {
	public Request() {
		super("request", "bungeeessentials.request", "helpop");
	}

	@Override
	public void execute(CommandSender commandSender, String[] args) {
		if (! (commandSender instanceof ProxiedPlayer)) {
			commandSender.sendMessage(new TextComponent("You must be a player to execute this command!"));
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer) commandSender;

		if (args.length > 0) {
			StringBuilder message = new StringBuilder();
			for (String word : args)
				message.append(word + " ");
			StaffEssentials.getInstance().broadcastStaffMessage(ConfigUtil.getConfig().getString("helpOpMessage")
					.replace("%name%", player.getName())
					.replace("%message%", message)
					.replace("%server%", player.getServer().getInfo().getName()));
		} else {
			player.sendMessage(new TextComponent(ConfigUtil.getConfig().getString("helpOpUsageMessage")));
		}
	}
}
