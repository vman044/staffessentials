package net.jmdev.staffessentials;

import net.jmdev.staffessentials.commands.Report;
import net.jmdev.staffessentials.commands.Request;
import net.jmdev.staffessentials.commands.StaffChat;
import net.jmdev.staffessentials.util.ConfigUtil;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/29/17 | 12:21 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class StaffEssentials extends Plugin {

	private static StaffEssentials plugin;

	@Override
	public void onEnable() {
		plugin = this;
		println("has been enabled!");
		if (!ConfigUtil.createConfig()) {
			ConfigUtil.loadConfig();
		}
		println("Loaded configuration file");
		loadCommands();
	}

	@Override
	public void onDisable() {
		println("has been disabled!");
	}

	public static StaffEssentials getInstance() {
		return plugin;
	}

	private void println(String msg) {
		getLogger().info(msg);
	}

	public void broadcastStaffMessage(String msg) {
		for (ProxiedPlayer player : getProxy().getPlayers()) {
			if (player.hasPermission("bungeeessentials.staff")) {
				player.sendMessage(new TextComponent(msg));
			}
		}
	}

	private void loadCommands() {
		getProxy().getPluginManager().registerCommand(this, new Report());
		getProxy().getPluginManager().registerCommand(this, new StaffChat());
		getProxy().getPluginManager().registerCommand(this, new Request());
	}

}
