package net.jmdev.staffessentials.util;

import net.jmdev.staffessentials.StaffEssentials;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/29/17 | 12:57 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class ConfigUtil {

	private static Configuration config;

	public static boolean createConfig() {
		File dataFolder = StaffEssentials.getInstance().getDataFolder();
		if (!dataFolder.exists())
			dataFolder.mkdir();

		File file = new File(dataFolder, "config.yml");

		if (!file.exists()) {
			try (InputStream in = StaffEssentials.getInstance().getResourceAsStream("config.yml")) {
				Files.copy(in, file.toPath());
				config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(dataFolder, "config.yml"));
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public static void loadConfig() {
		try {
			config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(StaffEssentials.getInstance().getDataFolder(), "config.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveConfig() {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).
					save(config, new File(StaffEssentials.getInstance().getDataFolder(),
							"config.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Configuration getConfig() {
		return config;
	}

}
